package ru.pab;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Andrey Platunov
 */

public class Application {

    private static String MESSAGE_NO_ARGS           = "No keywords were defined.";
    private static String MESSAGE_ERROR_FORMAT      = "'%s' : %s";
    private static String MESSAGE_SUCCESS_FORMAT    = "'%s' : %d";

    private static String MESSAGE_EMPTY_WORD        = "word is empty, wasn't processed";
    private static String MESSAGE_WHITESPACE_WORD   = "word consists of whitespaces only, wasn't processed";

    private static String URL                       = "https://news.yandex.ru/computers.html";

    public static void main(String... args) throws IOException {

        if (args.length == 0) {
            System.out.println(MESSAGE_NO_ARGS);
            return;
        }

        Document document = Jsoup.connect(URL).get();
        Elements elements = document.body().select("*");

        for (int i = 0; i < args.length; i++) {
            if (stringIsInvalid(args[i])) {
                continue;
            }

            int occurCount = 0;
            StringBuilder patternString = new StringBuilder()
                    .append("(?iu)\\b")
                    .append(args[i])
                    .append("\\b");

            Pattern pattern = Pattern.compile(patternString.toString());
            for (Element element : elements) {
                Matcher matcher = pattern.matcher(element.ownText());
                while(matcher.find()) {
                    occurCount++;
                }
            }
            System.out.println(String.format(MESSAGE_SUCCESS_FORMAT, args[i], occurCount));
        }
    }

    private static boolean stringIsInvalid(String string) {
        if (string == null || string.length() == 0) {
            System.out.println(String.format(MESSAGE_ERROR_FORMAT, string, MESSAGE_EMPTY_WORD));
            return true;
        }

        if (Pattern.compile("^[ \t\r\n]*$").matcher(string).matches()) {
            System.out.println(String.format(MESSAGE_ERROR_FORMAT, string, MESSAGE_WHITESPACE_WORD));
            return true;
        }

        return false;
    }
}
