### What is this?
This is console application founding words at page https://news.yandex.ru/computers.html.

### How to create executable jar file?
1. Clone current repo using git.
2. Make sure Java 8 and Maven installed.
3. Perform next command via console: 
    ```
    mvn clean compile assembly:single
    ```
4. By default result war is located at:
    ```
    <PROJECT_PATH>/target/words-searcher-1.0.0-SNAPSHOT-jar-with-dependencies.jar
    ```

###  How to execute jar?
1. Make sure Java 8 installed. You can execute jar using next command:
    ```
    java -jar <JAR_NAME>.jar <WORD_1> <WORD_2> ...
    ```
2. For example, performing command
    ```
    java -jar word-searcher.jar gadget computer
    ```
    may return next message:
    ```
    gadget: 15
    computer: 0
    ```
3. This message means that word "gadget" occurs 15 times, and word "computer" doesn't occur at the page.
    